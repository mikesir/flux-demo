# Flux Demo

This project is a simple demo of using the [Flux CI/CD Operator](https://fluxcd.io) for Kubernetes.

## Getting Started

Before using Flux, the Helm Operator needs to be installed and setup. This provides support for a `HelmRelease` resource type, allowing Helm charts to be defined and deployed using k8s objects.

```
kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/crds.yaml
kubectl create namespace flux
helm repo add fluxcd https://charts.fluxcd.io
helm upgrade -i helm-operator fluxcd/helm-operator \
    --namespace flux \
    --set helm.versions=v3
helm upgrade -i flux fluxcd/flux \
    --set git.url=git@code.vt.edu:mikesir/flux-demo.git \
    --set git.path=manifests \
    --set sync.interval=30s \
    --set "ssh.known_hosts=code.vt.edu ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPevFkLdVowVdwCNhZheKNbZnQmnTHzUpK+bl/4AOEflYeGKvBWNmM3oVCnG784ay1sGDDlMoAIlpyjwuj6/dZA=" \
    --namespace flux
```

Once deployed, get the SSH public key by using the command below and associate it with your Git repo.

```
kubectl -n flux logs deployment/flux | grep identity.pub | cut -d '"' -f2
```

